# NLP Practice

## Keepcoding project

Este proyecto consta de dos partes distintas. Por un lado tenemos un ejercicio de Topic Modeling y por otro lado hemos llevado a cabo un análisis de sentimiento.

Para la realización de esta práctica vamos a emplear dos datasets distintos.


## Topic Modeling
Para el Topic Modeling emplearemos un dataset de [twitter](https://www.kaggle.com/speckledpingu/RawTwitterFeeds). 

En este caso hemos empleado el csv de AllTweets con la intención de crear un  [modelo predictivo](https://gitlab.com/JorgeSola1/nlp-keepcoding-practice-twitter-amazon/-/blob/master/NlpModelTwitter.ipynb) que sea capaz de predicir quien es el autor del tweet en función del contenido del tweet y a la par, sacar un [Topic Modeling](https://gitlab.com/JorgeSola1/nlp-keepcoding-practice-twitter-amazon/-/blob/master/TopicModelingTwitter.ipynb). 

Los pasos a seguir para la creación del modelo predictivo y el topic modeling han sido:

####  [Procesamiento y limpieza de los tweets](https://gitlab.com/JorgeSola1/nlp-keepcoding-practice-twitter-amazon/-/blob/master/ProcessingTweets.ipynb)
Para ello he creado una función que es capaz de:
  * Eliminar símbolos y carácteres extraños.
  * Eliminar stopwords.
  *  Aprovechar la información de los hastags. Separar las palabras de forma adecuada.
  * Lematizar las palabras.
  * Convertir números en palabras.
  * Eliminar carácteres aislados.

Una vez han sido procesados todos los tweets, se crea un csv nuevo llamado "CleanAllTweets.csv" donde aparecen los tweets procesados y sus respectivos autores.

#### [Creación del modelo de predicción de autores](https://gitlab.com/JorgeSola1/nlp-keepcoding-practice-twitter-amazon/-/blob/master/NlpModelTwitter.ipynb)

Esta parte consiste en:
  * Leer el dataset procesado previamente.
  * Separar dicho dataset en train y test.
  * Normalizar los datos.
  * Emplear una regresión logística para crear un modelo predictivo
  * Entrenar dicho modelo.
  * Emplear el modelo para predecir los autores del dataset de test.

Los resultados obtenido son los siguientes:

author| precision | recall | f1-score| support 
| ------------- |:-------------:| -----:|-----:|-----:|
| AdamSavage       | 0.80 | 0.63 |0.71 | 1210 
| BarackObama | 0.80 | 0.79 |0.71 | 1745 
| DonaldTrump | 0.81 | 0.88 |0.84 | 4301 
| FiveThirtyEight | 0.74 | 0.81 |0.77 | 2375 
| HillaryClinton | 0.80 | 0.48 |0.60 | 855 
|KimKardashian |0.79| 0.88 |0.83| 2733 
|NASA |0.91 |0.96 |0.93 |3957 
|RichardDawkins |0.80 |0.78 |0.79 |1438 
|ScottKelly |0.91 |0.52 |0.66 |314 
|deGrasseTyson |0.82 |0.40 |0.54 |605 
|various |0.99 |0.98 |0.98 |2611 
|accuracy |0.84 |22144 
|macro avg |0.83 |0.74 |0.77 |22144 
|weighted avg |0.84 |0.84 |0.83 |22144

#### [Creación del Topic Modeling](https://gitlab.com/JorgeSola1/nlp-keepcoding-practice-twitter-amazon/-/blob/master/TopicModelingTwitter.ipynb)

Creamos una función para sacar los tokens de todos los tweets con la siguiente función:
```
def text_preprocessing(text): 
	result=[] 
	for token in gensim.utils.simple_preprocess(text): 
		if token not in gensim.parsing.preprocessing.STOPWORDS and len(token) > 3: 	
			result.append(token) 
	return result
```
Definimos el número de tokens en los que vamos a agrupar.
Para ello, analizamos si las palabras que aparecen agrupadas en cada token pertenecen a la misma temática.

Por otro lados tenemos el siguiente código para ver graficamente la distribuición de los tokens:
```
pyLDAvis.enable_notebook()
vis = pyLDAvis.gensim.prepare(lda_model, corpus, dictionary)
vis
```
La idea viendo las gráficas es comprobar que los tokens están separados entre si, es decir, que se han definido correctamente los diferentes grupos para abarcar todas los tokens y así poder diferenciar correctamente la temática de un tweet.

En este caso las temáticas que hay son:
  * Política Americana.
  * Espacio exterior.
  * News.
  * Comentarios.
  * Data, machine learning, análisis y tecnología.
  * Network.

## Sentiment Analysis 
Para el sentiment analysis emplearemos un dataset de [amazon](http://jmcauley.ucsd.edu/data/amazon/).

En este caso empleamos las reviews de Home and Kitchen.

La intención es crear un modelo predictivo binario que sea capaz de identificar si una review es positiva o negativa en función del contenido de dicha review.


Los pasos a seguir para crear el modelo sentiment analysis son:

#### [Procesamiento y limpieza de las reviews](https://gitlab.com/JorgeSola1/nlp-keepcoding-practice-twitter-amazon/-/blob/master/ProcessingReviews.ipynb)


El primer paso es descargar el json de Home and Kitchen y quedarnos con las columnas de "review text" y "overwall". 

El campo de overwall nos indica una calificación numérica del producto que en concuerda con el contenido de la review.

Es aconsejable limitar la cantidad de datos con la que vamos a trabajar, ya que el procesamiento de dichos datos puede llevar mucho tiempo.  Por ello en este caso empelamos únicamente 50.000 reviews. 

#### [Creación del modelo de sentiment analysis](https://gitlab.com/JorgeSola1/nlp-keepcoding-practice-twitter-amazon/-/blob/master/SentimentAnalysis.ipynb)

Esta parte consiste en:
  * Cargamos el dataset ya procesado.
  * Dividimos el dataset en train y test.
  * Llevamos a cabo la extracción de features.
  * Normalizamos los datos de train y test.
  * Creamos un modelo predictivo.
  * Entrenamos dicho modelo.




